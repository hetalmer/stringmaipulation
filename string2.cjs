// Function to convert string format of IPV4 address to integer array 
//eg 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].
function ipStrToInt(testString) {
    if (testString && typeof (testString) === 'string') {
        ip = testString.split(".");
        if (ip.length == 4) {
            iplist = [];
            for (let i = 0; i < 4; i++) {
                if (!isNaN(ip[i]) && ip[i] != "") {
                    iplist.push(Number(ip[i]));
                }
                else {
                    return [];
                }
            }
            return iplist;
        }
    }
    return [];
}
module.exports = ipStrToInt;