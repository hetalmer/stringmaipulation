const getMonth = require('../string3.cjs');
console.log(getMonth("01/12/2033"));
console.log(getMonth("01-22-2022"));
console.log(getMonth("December/10/1998"));
console.log(getMonth("11 22 2033"));
console.log(getMonth(""));
console.log(getMonth());