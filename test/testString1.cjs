const formatNumbers = require("../string1.cjs");
console.log(formatNumbers(["$10.5", "$30,000", "$80,00,000"]));
console.log(formatNumbers());
console.log(formatNumbers(1, 2, 3));
console.log(formatNumbers(["-$20.8", "$20,00,000", "-$23.9.9"]));
console.log(formatNumbers(["-$20.8f", "$20,00,000", "-$23.9"]));
console.log(formatNumbers(["-$88", "$20,00,000", "-$23.9"]));
console.log(formatNumbers(["-$20.8", "-$23"]));
console.log(formatNumbers([]));
