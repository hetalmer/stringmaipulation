//Function to get the month of entered date in string format
// format "18/1/2021"
const titleCase = require('./titlecase.cjs');
function getMonth(testString) {
    if (testString && typeof (testString) === 'string') {
        dt = testString.split(/[\s/-]/); // three seperator allowed for date is " ", / ,-
        return dt[0];
    }
    return "";
}
module.exports = getMonth;