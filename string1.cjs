//FormatNumber function is used to format the numbers which are stored as a string array. It removes the doller sign and comma 
function formatNumbers(testString) {
    if (testString && Array.isArray(testString) && testString.length) {
        let newList = [];
        for (let i = 0; i < testString.length; i++) {
            if (testString[i].startsWith("$") || testString[i].startsWith("-$")) {//check number starts with $ or not
                let data = testString[i].replace("$", ""); // replace $ symbol with blank
                data = data.replaceAll(",", "");// replace comma symbol with blank
                if (!isNaN(data)) {
                    newList.push(Number(data));// push all formated data to new numeric array
                }
                else {
                    return 0;
                }
            }
            else {
                return 0
            }
        }
        return newList;
    }
    return 0;
}
module.exports = formatNumbers;