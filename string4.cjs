//Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}
const titleCase = require('./titlecase.cjs');
function fullName(testString) {
    if (testString && typeof (testString) === 'object') {
        let fName = "";
        const keys = Object.keys(testString);
        if (!!testString) {
            for (let i = 0; i < keys.length; i++) {
                fName = fName.concat(titleCase(testString[keys[i]]), " ");//convert each value in title case
            }
        }
        return fName;
    }
    return "";
}
module.exports = fullName;