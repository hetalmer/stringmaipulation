//Function to combine the words of array into a one string.
// ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
function combineWords(testString) {
    if (!!testString && Array.isArray(testString)) {
        let fName = "";
        fName = testString.toString(); // converted string array to string with using toString function
        fName = fName.replaceAll(',', ' '); // It generated string with comma seperator, replace it with space
        fName += ".";//ends with . 
        return fName;
    }
    return "";
}
module.exports = combineWords;