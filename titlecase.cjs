//Function return the title case of a string passed as a argument
function titleCase(testString) {
    if (testString && typeof (testString) == 'string') {
        return testString.charAt(0).toUpperCase() + testString.substr(1).toLowerCase();
        //first it convert first character into upper case and than concate rest of character with lower case 
    }
}
module.exports = titleCase;